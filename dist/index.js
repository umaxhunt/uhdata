'use strict';Object.defineProperty(exports, "__esModule", { value: true });var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();function _asyncToGenerator(fn) {return function () {var gen = fn.apply(this, arguments);return new Promise(function (resolve, reject) {function step(key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {return Promise.resolve(value).then(function (value) {step("next", value);}, function (err) {step("throw", err);});}}return step("next");});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}var Sequelize = require('sequelize');var

Db = function () {
    function Db(config) {_classCallCheck(this, Db);
        var conf = config || Db.getConfig();
        this.sequelize = new Sequelize(conf.database, conf.username,
        conf.password,
        {
            port: conf.port,
            dialect: 'postgres' });


        this.repositories = {
            posts: this.sequelize.import('models/postsRepository') };

    }_createClass(Db, [{ key: 'close', value: function () {var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:if (!







                                this.sequelize) {_context.next = 4;break;}_context.next = 3;return (
                                    this.sequelize.close());case 3:
                                this.sequelize = undefined;case 4:case 'end':return _context.stop();}}}, _callee, this);}));function close() {return _ref.apply(this, arguments);}return close;}() }], [{ key: 'getConfig', value: function getConfig() {var env = process.env.NODE_ENV || 'dev';return require('./config/config.js')[env]; // eslint-disable-line global-require
        } }]);return Db;}();exports.default = Db;