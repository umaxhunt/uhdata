'use strict';var Sequelize = require('sequelize');

module.exports = function (sequelize) {
  var model = sequelize.define(
  'posts',
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true },

    ph_id: {
      type: Sequelize.INTEGER,
      unique: 'idx_ph_id' },

    createdAt: Sequelize.DATE,
    name: Sequelize.TEXT,
    tagline: Sequelize.TEXT,
    slug: Sequelize.STRING,
    votes_count: Sequelize.INTEGER,
    discussion_url: Sequelize.TEXT,
    redirect_url: Sequelize.TEXT,
    screenshot_url: Sequelize.TEXT,
    thumbnail_url: Sequelize.TEXT,
    topics: Sequelize.TEXT,

    // system fields
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE },

  {
    timestamps: true,
    underscored: true,
    indexes: [
    { unique: true, method: 'BTREE', fields: ['code'] }] });



  return model;
};