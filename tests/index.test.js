/* global it, describe, beforeEach, afterEach, expect */

const env = require('dotenv');

env.config();

import Db from '../src/index'; // eslint-disable-line import/first

const posts = require('./fixtures/example_response_posts.json')

const conf = Db.getConfig();

const { exec } = require('child_process');

function execPromise (cmd, options) {
    return new Promise(((resolve, reject) => {
        exec(cmd, options, (err, stdout) => {
            if (err) return reject(err);
            return resolve(stdout);
        });
    }));
}

function preparePost(post) {
  return {
    ph_id: post.id,
    createdAt: new Date(post.created_at),
    name: post.name,
    tagline: post.tagline,
    slug: post.slug,
    votes_count: post.votes_count,
    discussion_url: post.discussion_url,
    redirect_url: post.redirect_url,
    screenshot_url: post.screenshot_url['850px'],
    thumbnail_url: post.thumbnail.image_url,
    topics: post.topics.length ? post.topics.map(q=>q.name).join(',') : '',
  }
}


describe('developer tests', () => {
  beforeAll(async () => {
    if (process.env.RECREATE_DB === 'yes') {
      await execPromise(`SQL_DB_NAME=${conf.database} node_modules/.bin/sequelize db:create`, {env: process.env}) // eslint-disable-line max-len
            .then(() => execPromise(`SQL_DB_NAME=${conf.database} node_modules/.bin/sequelize db:migrate`)) // eslint-disable-line max-len
            .then(() => execPromise(`SQL_DB_NAME=${conf.database} node_modules/.bin/sequelize db:seed:all`)); // eslint-disable-line max-len
    }
  })

  // test('migration ok', async () => {
  //   const db = new Db(conf);

  //   const cnt = await db.repositories.posts.count();
  //   expect(cnt).toBeGreaterThanOrEqual(2);
  //   db.close();
  // });

  test('create-update-delete', async () => {
    const db = new Db(conf);
    const post = preparePost(posts.posts[0], db);
    await db.repositories.posts.insertOrUpdate(post);

    const post2 = preparePost(posts.posts[0], db);
    post2.tagline = 'qwe';
    await db.repositories.posts.insertOrUpdate(post2)

    const cnt = await db.repositories.posts.destroy({
      where: {ph_id: post.ph_id}
    })

    expect(cnt).toBe(1);

    await db.close();
  });

  test('bulk-upsert', async () => {
    const db = new Db(conf);
    const pposts = posts.posts.map(p => preparePost(p));
    const result = await Promise.all(
      pposts.map(p=>db.repositories.posts.insertOrUpdate(p)));
    
    expect(result.length).toBe(50);

    const post2 = preparePost(posts.posts[0], db);
    post2.tagline = 'qwe';
    await db.repositories.posts.insertOrUpdate(post2, 
      {
        updateOnDuplicate: ["ph_id"]
      })

    const cnt = await db.repositories.posts.destroy({
      where: {
        ph_id: {
          [db.sequelize.Op.in]: 
            posts.posts.map(p => p.id)
        }
      }
    })

    expect(cnt).toBeGreaterThanOrEqual(50);

    db.close();
  });

  // test('delete', async () => {
  //   const db = new Db(conf);
  //   db.close();
  // });

  afterAll(async() => {
    if (process.env.RECREATE_DB === 'yes') {
      await execPromise(`SQL_DB_NAME=${conf.database} node_modules/.bin/sequelize db:drop`); // eslint-disable-line max-len
    }
  })
});
