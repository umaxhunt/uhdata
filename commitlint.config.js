module.exports = {
  'rules': { 
    'references-empty': [2, 'never'],
  },
  parserPreset: {
    parserOpts: {
      issuePrefixes: ['umaxhunt/uhdata#', "umaxhunt/umaxhunt#",'umaxhunt/uhcrawler#']
    }
  },
};