function randomInt(low, high) {
  return Math.floor(Math.random() * (high - low) + low)
}

function getDbName() {
  if (process.env.SQL_DB_NAME) {
    return process.env.SQL_DB_NAME;
  }

  if (process.env.CI_DB_NAME) {
    return process.env.CI_DB_NAME;
  }

  if (process.env.RECREATE_DB === 'yes') {
    return `producthunt${randomInt(12345,54321)}`;
  }
    
   return 'producthunt';

}

module.exports = {
  "dev": {
    "username": process.env.DB_USERNAME || "postgres",
    "password": process.env.DB_PASSWORD || null,
    "database": getDbName(),
    "host": process.env.DB_HOST || "127.0.0.1",
    "port": process.env.DB_PORT || 5432,
    "dialect": "postgres",
    "seederStorageTableName": "sequelize_data"
  },
  "prod": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "dialect": "postgres",
    "seederStorageTableName": "sequelize_data"
  },
  "test": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": getDbName(),
    "host": process.env.DB_HOST,
    "port": process.env.DB_PORT,
    "dialect": "postgres",
    "seederStorageTableName": "sequelize_data"
  },
}
