const Sequelize = require('sequelize');

export default class Db {
    constructor(config) {
        const conf = config || Db.getConfig();
        this.sequelize = new Sequelize(conf.database, conf.username, 
            conf.password,
            {
                port: conf.port,
                dialect: 'postgres',
            });

        this.repositories = {
            posts: this.sequelize.import('models/postsRepository')
        }
    }

    static getConfig() {
        const env = process.env.NODE_ENV || 'dev';
        return require('./config/config.js')[env]; // eslint-disable-line global-require
    }

    async close() {
        if (this.sequelize) {
            await this.sequelize.close();
            this.sequelize = undefined;
        }
    }
}
