module.exports = {
  up: (queryInterface) => 
     queryInterface.bulkInsert('posts',[{
      ph_id: 133371,
      created_at: new Date(),
      createdAt: new Date(),
      updated_at: new Date(),
      name: 'Destroyer 666',
      tagline: 'destroys everything!',
      slug: 'destroyer123',
      votes_count: 99,
      discussion_url: 'https://ya.ru',
      redirect_url: 'http://yandex.ru',
      screenshot_url: 'https://avatars.mds.yandex.net/get-bunker/135516/58fe530fef36f215c0166b711cc2e87f02464940/orig',
      thumbnail_url: 'https://avatars.mds.yandex.net/get-banana/50379/x25Czp_70_h3ep5gW3KsdMmaS_banana_20161021_Castle-flagx402x.png/optimize',
      topics: 'krush,kill,destroy',              
    }, {
      ph_id: 133372,
      created_at: new Date(),
      createdAt: new Date(),
      updated_at: new Date(),
      name: 'Typeform Connect',
      tagline: 'Get your data moving',
      slug: 'typeform-connect',
      votes_count: 101,
      discussion_url: 'https://www.producthunt.com/posts/typeform-connect?utm_campaign=producthunt-api&utm_medium=api&utm_source=Application%3A+ohai+%28ID%3A+6451%29&qweqwe=eklowfownfowienfowienfowienfoiwenfoiwenfoiwenfoiwenfowienfowieinfowienfowienfowienfowienfowienfowienfowienfoweinfowienfowienfowienfoweinfowienfowienfowienfoweinfowinfoweinf',
      redirect_url: 'https://www.producthunt.com/r/8b92162f183025/135852?app_id=6451',
      screenshot_url: 'https://url2png.producthunt.com/v6/P5329C1FA0ECB6/13e10694e28d657baf406a71de3bcda3/png/?max_width=850&url=https%3A%2F%2Fwww.typeform.com%2Fconnect%3Futm_source%3DProductHunt%26utm_campaign%3DtypeformconnectPH',
      thumbnail_url: 'https://ph-files.imgix.net/0641ae4c-8f58-4bb3-8a93-e93b892ce9ab?auto=format&fit=crop&h=570&w=430',
      topics: 'Productivity,APIs,Marketing,Tech,Growth Hacking',              
    }], {})
  ,

  down: (queryInterface, Sequelize) => 
     queryInterface.bulkDelete('posts', 
      {[Sequelize.Op.or]: [{ph_id: 133371}, {ph_id: 133372}]}, {})
  
};
