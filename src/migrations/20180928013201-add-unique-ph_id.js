module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addIndex('posts', {
      fields: ['ph_id'],
      unique: true,
      name: 'idx_ph_id'
    });
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeIndex('posts','idx_ph_id');
  }
};
