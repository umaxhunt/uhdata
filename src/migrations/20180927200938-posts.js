module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('posts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ph_id: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.TEXT
      },
      tagline: {
        type: Sequelize.TEXT
      },
      slug: {
        type: Sequelize.STRING
      },
      votes_count: {
        type: Sequelize.INTEGER
      },
      discussion_url: {
        type: Sequelize.TEXT
      },
      redirect_url: {
        type: Sequelize.TEXT
      },
      screenshot_url: {
        type: Sequelize.TEXT
      },
      thumbnail_url: {
        type: Sequelize.TEXT
      },
      topics: {
        type: Sequelize.TEXT
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
    }),
  down: (queryInterface) => queryInterface.dropTable('posts')
};